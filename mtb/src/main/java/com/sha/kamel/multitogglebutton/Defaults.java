package com.sha.kamel.multitogglebutton;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

public interface Defaults {
    float DEFAULT_CORNER_RADIUS = 18;

    default int color(AttrSet attrs, String res) {
        return AttrUtils.getColorFromAttr(attrs, res, 0);
    }

    default int color(AttrSet attrs, String res, int defValue) {
        return AttrUtils.getColorFromAttr(attrs, res, defValue);
    }

    default int color(int res) {
        try {
            return getContext().getResourceManager().getElement(res).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    default Context getContext() {
        throw new UnsupportedOperationException();
    }

    default LayoutScatter layoutInflater() {
        return LayoutScatter.getInstance(getContext());
    }

    default boolean isValidColor(int color) {
        return color != 0;
    }

    default void setBackground(Component v, int color) {
        if (!(v.getBackgroundElement() instanceof ShapeElement)) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
            v.setBackground(shapeElement);
            return;
        }
        ShapeElement drawable = (ShapeElement) v.getBackgroundElement();
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
    }

    default ShapeElement gradientDrawable(Component v) {
        return v.getBackgroundElement() instanceof ShapeElement
                ? (ShapeElement) v.getBackgroundElement()
                : new ShapeElement();
    }

    default void radius(Component v, float radius) {
        if (radius == -1) radius = DEFAULT_CORNER_RADIUS;
        ShapeElement drawable = gradientDrawable(v);
        RgbColor rgbColor = RgbColor.fromArgbInt(Color.TRANSPARENT.getValue());
        drawable.setStroke(0, rgbColor);
        drawable.setCornerRadius(radius);
        v.setBackground(drawable);
    }

    default void radius(Component v, float[] radius) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadiiArray(radius);
        RgbColor rgbColor = RgbColor.fromArgbInt(Color.TRANSPARENT.getValue());
        shapeElement.setStroke(0, rgbColor);
        v.setBackground(shapeElement);
    }

    default void leftRadius(Component v, float radius) {
        if (radius == -1) radius = DEFAULT_CORNER_RADIUS;
        radius(v, new float[] {radius, radius, 0, 0, 0, 0, radius, radius});
    }

    default void rightRadius(Component v, float radius) {
        if (radius == -1) radius = DEFAULT_CORNER_RADIUS;
        radius(v, new float[] {0, 0, radius, radius, radius, radius, 0, 0});
    }

    default <T> int count(T[] array) {
        return array == null ? 0 : array.length;
    }

    default int count(int[] array) {
        return array == null ? 0 : array.length;
    }

    default boolean isEmpty(int[] array) {
        return array == null || array.length == 0;
    }

    default boolean isNotEmpty(int[] array) {
        return array != null && array.length > 0;
    }

    default boolean isEmpty(boolean[] array) {
        return array == null || array.length == 0;
    }

    default boolean isNotEmpty(boolean[] array) {
        return array != null && array.length > 0;
    }

    default <T> boolean isEmpty(List<T> array) {
        return array == null || array.isEmpty();
    }

    default <T> boolean isNotEmpty(List<T> array) {
        return array != null && !array.isEmpty();
    }

    default <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    default <T> boolean isNotEmpty(T[] array) {
        return array != null && array.length > 0;
    }

    default <T> int count(List<T> list) {
        return list == null ? 0 : list.size();
    }

    default String text(Text tv) {
        return tv.getText();
    }

    default void setFormatText(Text text, String str, Boolean textAllCaps) {
        if (textAllCaps) {
            str = str.toUpperCase();
        }
        text.setText(str);
    }

    default void setFormatText(Text text, int resId, Boolean textAllCaps) {
        try {
            String str = getContext().getResourceManager().getElement(resId).getString();
            if (textAllCaps) {
                str = str.toUpperCase();
            }
            text.setText(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
