package com.sha.kamel.multitogglebutton;

import com.annimon.stream.Stream;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class MultiToggleButton extends ToggleButton {

    public MultiToggleButton(Context context) {
        super(context, null);
    }

    public MultiToggleButton(Context context, AttrSet attrs) {
        super(context, attrs);
        initItems();
    }

    private void initItems() {
        setItems(getLabels());
    }

    /**
     * Set the enabled state of this MultiToggleButton, including all of its child items.
     *
     * @param enabled True if this view is enabled, false otherwise.
     */
    @Override
    public void setEnabled(boolean enabled) {
        Stream.of(items).forEach(item -> item.setEnabled(enabled));
    }

    /**
     * Set multiple items with the specified labels
     *
     * @param labels An array of CharSequences for the items
     * @return this
     */
    public ToggleButton setItems(CharSequence[] labels) {
        return setItems(labels, null, null);
    }

    /**
     * Set multiple items with the specified labels
     *
     * @param labels An array of CharSequences for the items
     * @return this
     */
    public ToggleButton setItems(List<CharSequence> labels) {
        return setItems(labels.toArray(new CharSequence[0]), null, null);
    }

    /**
     * Set multiple items with the specified labels and default initial values. Initial states are
     * allowed, but both arrays must be of the same size.
     *
     * @param labels An array of CharSequences for the items
     * @param selected The default value for the items
     * @return this
     */
    public ToggleButton setItems(CharSequence[] labels, boolean[] selected) {
        return setItems(labels, null, selected);
    }

    /**
     * Set multiple items with the specified labels and default initial values. Initial states are
     * allowed, but both arrays must be of the same size.
     *
     * @param labels An array of CharSequences for the items
     * @param imageResourceIds an optional icon to show, either text, icon or both needs to be set.
     * @param selected The default value for the items
     * @return this
     */
    public ToggleButton setItems(
            CharSequence[] labels, int[] imageResourceIds, boolean[] selected) {
        boolean[] selection = selected == null ? new boolean[count(labels)] : selected;
        if (!isEmpty(items) && selectFirstItem && selection.length > 0) {
            selection[0] = true;
            notifyItemSelected(
                    items.get(0),
                    true,
                    0,
                    isEmpty(labels) ? items.get(0).getText() : labels[0].toString());
        }
        setLabels(labels);
        final int itemsCount = Math.max(count(labels), count(imageResourceIds));

        if (itemsCount == 0) return this;

        prepare();

        addItems(itemsCount, labels, imageResourceIds, selection);
        if (hasRoundedCorners()) radius(rootView, cornerRadius);
        return this;
    }

    private void addItems(
            int itemsCount, CharSequence[] labels, int[] imageResourceIds, boolean[] selected) {
        rootView.removeAllComponents();
        items = new ArrayList<>(itemsCount);

        if (labels == null) return;

        Stream.of(labels)
                .forEachIndexed(
                        (i, label) -> {
                            Text tv = createTextView(i, itemsCount);
                            setFormatText(tv, label.toString(), textAllCaps);
                            //                    tv.setText(label.toString().toUpperCase());
                            //                    if (imageResourceIds != null &&
                            // imageResourceIds[i] != 0) {
                            //                        //
                            // tv.setCompoundDrawablesWithIntrinsicBounds(imageResourceIds[i], 0, 0,
                            // 0);
                            //                        //tv.setAroundElements(imageResourceIds[i], 0,
                            // 0, 0);
                            //                    }

                            tv.setClickedListener(
                                    v -> {
                                        if (!v.isSelected()
                                                && maxItemsToSelect > 0
                                                && getSelectedItemsSize() == maxItemsToSelect) {
                                            maxCallback.accept(maxItemsToSelect);
                                            return;
                                        }
                                        toggleItemSelection(i);
                                    });
                            rootView.addComponent(tv);

                            boolean defaultSelected = true;
                            if (selected == null || itemsCount != selected.length)
                                defaultSelected = false;
                            if (defaultSelected) setItemSelected(tv, selected[i]);

                            this.items.add(tv);
                        });
    }

    @Override
    public ToggleButton setLabelsRes(List<Integer> labels) {
        List<CharSequence> l =
                Stream.of(labels)
                        .map(label -> getContext().getString(label))
                        .map(item -> (CharSequence) item)
                        .toList();
        setItems(l);
        return super.setLabelsRes(labels);
    }

    @Override
    public ToggleButton setLabels(List<String> labels) {

        return super.setLabels(labels);
    }
}
