package com.sha.kamel.togglesample.sample;

import com.sha.kamel.multitogglebutton.MultiToggleButton;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private final Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.sha.kamel.togglesample.sample", actualBundleName);
    }

    @Test
    public void getYesNoArray() throws NotExistException, WrongTypeException, IOException {
        String[] stringArray =
                context.getResourceManager()
                        .getElement(ResourceTable.Strarray_yes_no_array)
                        .getStringArray();
        assertEquals(2, stringArray.length);
    }

    @Test
    public void getPlanetsArray() throws NotExistException, WrongTypeException, IOException {
        String[] stringArray =
                context.getResourceManager()
                        .getElement(ResourceTable.Strarray_planets_array)
                        .getStringArray();
        assertEquals(4, stringArray.length);
    }

    @Test
    public void getDogsArray() throws NotExistException, WrongTypeException, IOException {
        String[] stringArray =
                context.getResourceManager()
                        .getElement(ResourceTable.Strarray_dogs_array)
                        .getStringArray();
        assertEquals(5, stringArray.length);
    }

    @Test
    public void testMultiToggleButton() throws NotExistException, WrongTypeException, IOException {
        String[] dogsArray =
                context.getResourceManager()
                        .getElement(ResourceTable.Strarray_dogs_array)
                        .getStringArray();
        MultiToggleButton multiToggleButton = new MultiToggleButton(context);
        multiToggleButton
                .setItems(dogsArray, null, new boolean[dogsArray.length])
                .setPressedColorTextRes(ResourceTable.Color_white)
                .setUnpressedColorTextRes(ResourceTable.Color_white);
        CharSequence[] labels = multiToggleButton.getLabels();
        assertArrayEquals(labels, dogsArray);
        boolean[] itemsSelection = new boolean[] {false, true, false, false, true};
        multiToggleButton.setItemsSelection(itemsSelection);
        assertArrayEquals(itemsSelection, multiToggleButton.getSelectionArray());
    }
}
