package com.sha.kamel.togglesample.sample.slice;

import com.annimon.stream.Stream;
import com.sha.kamel.multitogglebutton.*;
import com.sha.kamel.togglesample.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.global.resource.ResourceManager;

import java.util.Arrays;

public class MainAbilitySlice extends AbilitySlice {
    private StackLayout mSlMore;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(Color.rgb(0, 0, 0));
        initMore();
        MultiToggleButton mtb1 = (MultiToggleButton) findComponentById(ResourceTable.Id_mtb1);
        MultiToggleButton mtb2 = (MultiToggleButton) findComponentById(ResourceTable.Id_mtb2);
        MultiToggleButton mtb3 = (MultiToggleButton) findComponentById(ResourceTable.Id_mtb3);
        MultiToggleButton mtb4 = (MultiToggleButton) findComponentById(ResourceTable.Id_mtb4);
        MultiToggleButton mtb5 = (MultiToggleButton) findComponentById(ResourceTable.Id_mtb5);
        ResourceManager resourceManager = getResourceManager();

        try {
            String[] yesNoArray =
                    resourceManager
                            .getElement(ResourceTable.Strarray_yes_no_array)
                            .getStringArray();
            mtb1.setItems(yesNoArray);
            mtb1.setOnItemSelectedListener(listener()).setLabel("Yes", 0);

            mtb2.setItems(yesNoArray);
            mtb2.setOnItemSelectedListener(listener())
                    .setLabelsRes(
                            Arrays.asList(ResourceTable.String_left, ResourceTable.String_right))
                    .setColorRes(ResourceTable.Color_mtb_green, ResourceTable.Color_mtb_gray)
                    .setCornerRadius(40);

            String[] planetsArray =
                    resourceManager
                            .getElement(ResourceTable.Strarray_planets_array)
                            .getStringArray();
            mtb3.setItems(planetsArray);
            mtb3.setOnItemSelectedListener(listener());

            String[] dogsArray =
                    resourceManager.getElement(ResourceTable.Strarray_dogs_array).getStringArray();
            mtb4.setItems(dogsArray);
            mtb4.setOnItemSelectedListener(listener())
                    .maxSelectedItems(2, max -> toast("Can't select more than " + max + " items."));

            mtb5.setItems(dogsArray, null, new boolean[dogsArray.length])
                    .setOnItemSelectedListener(listener())
                    .setPressedColorTextRes(ResourceTable.Color_white)
                    .setUnpressedColorTextRes(ResourceTable.Color_white)
                    .setOnItemSelectedListener(listener());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initMore() {
        mSlMore = (StackLayout) findComponentById(ResourceTable.Id_sl_more);
        findComponentById(ResourceTable.Id_iv_more)
                .setClickedListener(
                        component -> {
                            if (mSlMore.getVisibility() == Component.HIDE) {
                                mSlMore.setVisibility(Component.VISIBLE);
                            } else {
                                mSlMore.setVisibility(Component.HIDE);
                            }
                        });

        findComponentById(ResourceTable.Id_dl_root)
                .setClickedListener(
                        component -> {
                            if (mSlMore.getVisibility() == Component.VISIBLE) {
                                mSlMore.setVisibility(Component.HIDE);
                            }
                        });
    }

    private ToggleButton.OnItemSelectedListener listener() {
        return (toggleButton, item, index, label, selected) -> {
            String msg =
                    "Number "
                            + index
                            + " is "
                            + (selected ? "selected" : "deselected")
                            + ", Label: "
                            + label
                            + ", Selected items: "
                            + selectedItemsMsg(toggleButton);
            toast(msg);
        };
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private String selectedItemsMsg(ToggleButton toggleButton) {
        Selected selected = toggleButton.getSelected();
        String msg;
        if (selected.isAnySelected()) {
            if (selected.isSingleItem())
                msg = "One item selected: " + selected.getSingleItemPosition();
            else
                msg =
                        Stream.of(selected.getSelectedPositions())
                                .map(String::valueOf)
                                .reduce((p1, p2) -> p1 + ", " + p2)
                                .get();

        } else msg = "No items selected";
        return msg;
    }

    private void toast(String s) {
        LogUtil.loge(s);
    }
}
