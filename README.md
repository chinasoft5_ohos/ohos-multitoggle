# ohos-multitoggle

## 项目介绍
- 项目名称：ohos-multitoggle
- 所属系列：openharmony的第三方组件适配移植
- 功能：滚轮控件，基于滑动列表实现，可以自定义样式
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：1.8.3@aar
## 效果演示
 ![Image text](./gifs/multitoggle.gif)
## 安装教程

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

        implementation('com.gitee.chinasoft_ohos:multitogglebutton:1.0.0')

    ......  

 }

 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明

1.将仓库导入到本地仓库中

2.新增MainAbility继承Ability,代码实例如下:
##### Java:
```Java
    public class MainAbility extends Ability {
        @Override
        public void onStart(Intent intent) {
            super.onStart(intent);
            setUIContent(ResourceTable.Layout_ability_main);
            MultiToggleButton mtb1 = (MultiToggleButton) findComponentById(ResourceTable.Id_mtb1);

             String[] yesNoArray =
                                resourceManager
                                        .getElement(ResourceTable.Strarray_yes_no_array)
                                        .getStringArray();
             mtb1.setItems(yesNoArray);
             mtb1.setOnItemSelectedListener(listener()).setLabel("Yes", 0);

        }
    }
```

##### XML:
```xml
   <com.sha.kamel.multitogglebutton.MultiToggleButton
              ohos:id="$+id:mtb"
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:top_margin="10vp"
              app:mtbColorPressedText="$color:white"
              app:mtbColorUnpressedText="$color:white"
              app:mtbPressedColor="$color:orange_pressed"
              app:mtbRoundedCorners="true"
              app:mtbScrollable="true"
              app:mtbUnpressedColor="$color:orange"
              />
```
##### 设置Item监听
```java
 mtb.setOnItemSelectedListener((toggleButton, item, index, label, selected) -> {  
  toast(selected ? "selected" : "deselected"));  
});
```
##### 获取选中的items
您可以获得选定的Item。
```java
Selected selected = toggleButton.getSelected();

boolean isAnySelected = selected.isAnySelected();
boolean isAllSelected = selected.isAllSelected();
boolean isSingleItem = selected.isSingleItem();

int singleItemPosition = selected.getSingleItemPosition();
TextView singleItem = selected.getSingleItem();

List<TextView> selectedItems = selected.getSelectedItems();
List<Integer> selectedPositions = selected.getSelectedPositions();
```
##### 设置颜色
您可以为不同的状态选择任何想要的颜色。
```java
mtb.setColorRes(R.color.mtb_green, R.color.mtb_gray);
```
设置颜色有很多方法。看一下 `ToggleButton`

#####  圆角
你可以设置圆角:
```java
mtb.setRoundedCorners();
```

##### 圆角
可以设置默认圆角值为 `20vp` :
```java
mtb.setCornerRadius(20);
```


###### 注意:
如果你设置角半径为 `setCornerRadius`,不需要设置 `setRoundedCorners`.

##### 多选
```java
mtb.multipleChoice(true)
```

#####最大选择item数
您可以设置允许选择的最大项目
```java
mtb.maxSelectedItems(2, max -> toast("Can't select more than " + max + " items."));
```
###### 注意：
如果设置 `maxSelectedItems`, 不需要设置 `multipleChoice(true)`.

##### Scroll
如果`MultiToggleButton` 需要滚动，可以这样设置
```java
mtb.setScrollable(true);
```
###### 注意：
默认情况下，滚动是禁用的。


##### See 'app' module for the full code.

## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT



## 版权和许可信息  

Apache license 2.0
